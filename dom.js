/*
 1. Для цього необхідно створити елемент,
встановити йому необхідні атрибути та текстовий контент,
і прикріпити його до DOM дерева.
 2. beforebegin - вставляє HTML-код перед елементом;
    1) afterbegin - вставляє HTML-код в середину елементу, перед його вмістом;
    2) beforeend - вставляє HTML-код в середину елементу, після його вмісту;
    3) afterend - вставляє HTML-код після елемента.
 3.Є кілька способів видалення елемента зі сторінки:
    1) Використання методу 'remove'
    2) Використання методу 'parentNode.removeChild()'
    3) Використання методу 'replaceWith'
*/

function displayList(arr, parent = document.body) {
  const list = document.createElement("ul");
  parent.appendChild(list);
  arr.forEach((item) => {
    const listItem = document.createElement("li");
    if (Array.isArray(item)) {
      displayList(item, listItem);
    } else {
      listItem.textContent = item;
    }
    list.appendChild(listItem);
  });
}

const myArray = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];
displayList(myArray);

let count = 3;
const countdown = setInterval(() => {
  console.log(count);
  count--;
}, 1000);

setTimeout(() => {
  clearInterval(countdown);
  document.body.innerHTML = "";
}, 3000);
